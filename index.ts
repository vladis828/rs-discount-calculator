interface Arguments {
  price: number;
  discount: number;
  isInstallment: boolean;
  months: number;
}

const totalPrice = ({
  price,
  discount,
  isInstallment,
  months,
}: Arguments): number => {
  return (price - (price * discount) / 100) / (isInstallment ? months : 1);
};

console.log(
  totalPrice({ price: 100000, discount: 25, isInstallment: true, months: 12 })
);
// 6250
